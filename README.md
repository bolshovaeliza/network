# Network

This project is an introduction to network configuration using an example of virtual machines. The `report.md` file contains examples of output of certain tools, as well as details on netplan and firewall configuration.
