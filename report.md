# REPORT: Сети в Linux

1. [Инструмент ipcalc](#part-1-инструмент-ipcalc) 
2. [Статическая маршрутизация между двумя машинами](#part-2-статическая-маршрутизация-между-двумя-машинами) 
3. [Утилита iperf3](#part-3-утилита-iperf3) 
4. [Сетевой экран](#part-4-сетевой-экран) 
5. [Статическая маршрутизация сети](#part-5-статическая-маршрутизация-сети) 
6. [Динамическая настройка IP с помощью DHCP](#part-6-динамическая-настройка-ip-с-помощью-dhcp) 
7. [NAT](#part-7-nat) 
8. [Допополнительно. Знакомство с SSH Tunnels](#part-8-дополнительно-знакомство-с-ssh-tunnels)


## Part 1. Инструмент **ipcalc**

### Определение адреса:
- Адрес сети *192.167.38.54/13*

![192...](imgs/DO2_1-1.png)

### Перевод маски
Таблица для перевода ([источник](https://www.connecteddots.online/resources/blog/subnet-masks-table))

![table1](imgs/DO2_1-table-1.png)

![table2](imgs/DO2_1-table-2.png)
- Перевод маски *255.255.255.0*
   - Префиксная запись: /24
   - Двоичная запись: 11111111.11111111.11111111.00000000
- Перевод маски */15* 
   - Двоичная запись: 11111111.11111110.00000000.00000000
   - Обычная запись: 255.254.0.0
- Перевод маски *11111111.11111111.11111111.11110000*
   - Обычная запись: 255.255.255.240
   - Префиксная запись: /28

### Поиск минимального и максимального хоста

Сеть *12.167.38.4* 
Маски: 
- */8* \
   HostMin: 12.0.0.1 \
   HostMax: 12.255.255.254 \
   ![minmaxhost-1](imgs/DO2_1-2.png)
- *11111111.11111111.00000000.00000000* -> 255.255.0.0 \
   HostMin: 12.167.0.1 \
   HostMax: 12.167.255.254 \
   ![minmaxhost-2](imgs/DO2_1-3.png)
- *255.255.254.0* \
   HostMin: 12.167.38.1 \
   HostMax: 12.167.39.254 \
   ![minmaxhost-3](imgs/DO2_1-4.png)
- */4* \
   HostMin: 0.0.0.1 \
   HostMax: 15.255.255.254 \
   ![minmaxhost-4](imgs/DO2_1-5.png)


### Обращение к приложению с помощью некоторых IP

- IP *194.34.23.100*: нельзя (нет loopback)
   ![localhost-1](imgs/DO2_1-6.png)
- IP *127.0.0.2*: можно (есть loopback)
   ![localhost-2](imgs/DO2_1-7.png)
- IP *127.1.0.1*: можно (есть loopback)
   ![localhost-3](imgs/DO2_1-8.png)
- IP *128.0.0.1*:нельзя (нет loopback)
   ![localhost-4](imgs/DO2_1-9.png)

### Определение частных и публичных IP

*10.0.0.45*: Частный \
![public/private-1](imgs/DO2_1-10.png) \
*134.43.0.2*: Публичный \
![public/private-2](imgs/DO2_1-11.png) \
*192.168.4.2*: Частный \
![public/private-3](imgs/DO2_1-12.png) \
*172.20.250.4*: Частный \
![public/private-4](imgs/DO2_1-13.png) \
*172.0.2.1*: Публичный \
![public/private-5](imgs/DO2_1-14.png) \
*192.172.0.1*: Публичный \
![public/private-6](imgs/DO2_1-15.png) \
*172.68.0.2*: Публичный \
![public/private-7](imgs/DO2_1-16.png) \
*172.16.255.255*: Частный \
![public/private-8](imgs/DO2_1-17.png) \
*10.10.10.10*: Частный \
![public/private-9](imgs/DO2_1-18.png) \
*192.169.168.1*: Публичный \
![public/private-10](imgs/DO2_1-19.png) 


### Определение возможных IP адресов у сети *10.10.0.0/18*

![determine-possible-ips](imgs/DO2_1-20.png) \

Минимальный хост - 10.10.0.1, максимальный хост - 10.10.63.254. В этот диапазон попадают следующие адреса: *10.10.0.2*, *10.10.10.10*, *10.10.1.255*. \
*10.10.0.1* обычно роутер, его не стоит использовать.

---
---

## Part 2. Статическая маршрутизация между двумя машинами

### Доступные интерфейсы
Для ws1 \
![ws1-interfaces](imgs/DO2_2-1-ws1.png) \
Для ws2 \
![ws2-interfaces](imgs/DO2_2-1-ws2.png)

> А вот здесь только я включила еще один интерфейс в настройках VirtualBox :) \
По-хорошему, еще должен быть интерфейс *enp0s8*.

### Задание адресов вручную
В файл *etc/netplan/00-installer-config.yaml* для каждой машины задать адреса и маски.

Для ws1 (адрес и маска: *192.168.100.10/16*)\
![ws1-address](imgs/DO2_2-2-ws1.png) \
Для ws2 (адрес и маска: *172.24.116.8/12*) \
![ws2-address](imgs/DO2_2-2-ws2.png) 

### Перезапуск сети

Результат выполнения команды `sudo netplan apply` \
Для ws1 \
![ws1-netplan](imgs/DO2_2-3-ws1.png) \
Для ws2 \
![ws2-netplan](imgs/DO2_2-3-ws2.png) 

### Добавление статического маршрута вручную

Результат выполнения команды `sudo ip route add <address> dev enp0s8` \
Для ws1 \
![ws1-static-route](imgs/DO2_2-4-ws1.png) \
Для ws2 \
![ws2-static-route](imgs/DO2_2-4-ws2.png) 

### Добавление статического маршрута с сохранением

Сначала нужно перезапустить машину с помощью `sudo reboot`, затем редактирование файла на обеих машинах: `sudo vim /etc/netplan/00-installer-config.yaml` \
Для ws1 \
![ws1-static-route-save](imgs/DO2_2-5-ws1.png) \
Для ws2 \
![ws2-static-route-save](imgs/DO2_2-5-ws2.png) \
После обновления нужно снова использовать команду `sudo netplan apply` \
Результат пинга: \
Для ws1 \
![ws1-ping](imgs/DO2_2-6-ws1.png) \
Для ws2 \
![ws2-ping](imgs/DO2_2-6-ws2.png) =

---
---

## Part 3. Утилита **iperf3**

### Перевод единиц измерения
8 Mbps => 1 MB/s\
100 MB/s => 800000 Kbps\
1 Gbps => 1000 Mbps

### Измерение скорости соединения между ws1 и ws2
Клиентская сторона (`iperf3 -c 172.24.116.8`) \
![ws1-iperf3-client](imgs/DO2_3-1-ws1.png) \
Серверная сторона (`iperf3 -s -f m`) \
![ws2-iperf3-server](imgs/DO2_3-1-ws2.png) 

---
---

## Part 4. Сетевой экран

### Использование **iptables** для создания файервола
Файервол ws1: \
![ws1-iptables-firewall](imgs/DO2_4-1-ws1.png) \
Правила `iptables`: \
![ws1-iptables](imgs/DO2_4-2-ws1.png) 

Файервол ws2: \
![ws2-iptables-firewall](imgs/DO2_4-1-ws2.png) \
Правила `iptables`: \
![ws1-iptables](imgs/DO2_4-2-ws2.png) 

Правила `iptables` применяются поочереди, поэтому будет применено последнее правило в случае нескольких противоречащих. Это легко продемонстрировать с помощью пинга. \
Пинг ws1:  \
![ws1-nmap](imgs/DO2_4-3-ws2.png) \
Пинг ws2:  \
![ws1-nmap](imgs/DO2_4-3-ws1.png) 

Как можно заметить, ws1 пингуется, а ws2 нет. С помощью `nmap` проверяем, что `Host is up`.

Сохранение образов виртуальных машин можно выполнять через интерфейс продукта визуализации (в моем случае VirtualBox).
![dump-create](imgs/DO2_4-4.png) 

---
---
## Part 5. Статическая маршрутизация сети

### Настройка адресов машин

#### WS11
Конфигурация `etc/netplan/00-installer-config.yaml`\
![ws11-netplan-config](imgs/DO2_5-1-ws11.png) \
Проверка адреса машины после перезапуска сервиса (`sudo netplan apply`)\ 
![ws11-address](imgs/DO2_5-2-ws11.png) 

#### WS21
Конфигурация `etc/netplan/00-installer-config.yaml`\
![ws11-netplan-config](imgs/DO2_5-1-ws21.png) \
Проверка адреса машины после перезапуска сервиса (`sudo netplan apply`)\ 
![ws11-address](imgs/DO2_5-2-ws21.png) 

#### WS22
Конфигурация `etc/netplan/00-installer-config.yaml`\
![ws11-netplan-config](imgs/DO2_5-1-ws22.png) \
Проверка адреса машины после перезапуска сервиса (`sudo netplan apply`)\ 
![ws11-address](imgs/DO2_5-2-ws22.png) 

#### R1
Конфигурация `etc/netplan/00-installer-config.yaml`\
![ws11-netplan-config](imgs/DO2_5-1-r1.png) \
Проверка адреса машины после перезапуска сервиса (`sudo netplan apply`)\ 
![ws11-address](imgs/DO2_5-2-r1.png) 

#### R2
Конфигурация `etc/netplan/00-installer-config.yaml`\
![ws11-netplan-config](imgs/DO2_5-1-r2.png) \
Проверка адреса машины после перезапуска сервиса (`sudo netplan apply`)\ 
![ws11-address](imgs/DO2_5-2-r2.png) 

#### **Приём, проверка связи**
Пинг `ws22` с `ws21`\
![ws21-ping](imgs/DO2_5-3-ws21.png) \
Пинг `r1` с `ws11`\
![ws11-ping](imgs/DO2_5-3-ws11.png) 

### Установка переадресации
#### Сначала одной командой
А именно `sudo sysctl -w net.ipv4.ip_forward=1` \
Для r1: \
![r1-ip-forward-temp](imgs/DO2_5-3-r1.png) \
Для r2: \
![r2-ip-forward-temp](imgs/DO2_5-3-r2.png) 

#### А теперь понадежнее
Нужно с помощью `sudo vim /etc/sysctl.conf` добавить к файлу следующую строку: `net.ipv4.ip_forward = 1` \
Для r1: \
![r1-ip-forward](imgs/DO2_5-4-r1.png) \
Для r2: \
![r2-ip-forward](imgs/DO2_5-4-r2.png) \

### Настройка шлюза по-умолчанию

#### WS11
Конфигурация `etc/netplan/00-installer-config.yaml`\
![ws11-netplan-config-gateway](imgs/DO2_5-5-ws11.png) \
Проверка шлюзов после перезапуска сервиса (`sudo netplan apply`) с помощью `ip r`  
![ws11-gateway](imgs/DO2_5-6-ws11.png) 

#### WS21
Конфигурация `etc/netplan/00-installer-config.yaml`\
![ws21-netplan-config-gateway](imgs/DO2_5-5-ws21.png) \
Проверка шлюзов после перезапуска сервиса (`sudo netplan apply`) с помощью `ip r` \ 
![ws21-gateway](imgs/DO2_5-6-ws21.png)

#### WS22
Конфигурация `etc/netplan/00-installer-config.yaml`\
![ws22-netplan-config-gateway](imgs/DO2_5-5-ws22.png) \
Проверка шлюзов после перезапуска сервиса (`sudo netplan apply`) с помощью `ip r` \ 
![ws22-gateway](imgs/DO2_5-6-ws22.png)

#### R1
Конфигурация `etc/netplan/00-installer-config.yaml`\
![r1-netplan-config-gateway](imgs/DO2_5-5-r1.png) \
Проверка шлюзов после перезапуска сервиса (`sudo netplan apply`) с помощью `ip r` \ 
![r1-gateway](imgs/DO2_5-6-r1.png)

#### R2
Конфигурация `etc/netplan/00-installer-config.yaml`\
![r2-netplan-config-gateway](imgs/DO2_5-5-r2.png) \
Проверка шлюзов после перезапуска сервиса (`sudo netplan apply`) с помощью `ip r` 
![r2-gateway](imgs/DO2_5-6-r2.png)


#### **Проверка связи (пинг r2 с ws11)**
Со стороны ws11 (`ping 10.100.0.12`): \
![ws11-ping](imgs/DO2_5-7-ws11.png) \
Со стороны r2 (`tcpdump -tn -i enp0s8`): \
![r2-ping](imgs/DO2_5-7-r2.png)

### Добавление статических маршрутов
Конфигурация для r1. \
![r1-static-route](imgs/DO2_5-8-r1.png) \
Итоговая таблица с маршрутами. \
![r1-static-table](imgs/DO2_5-9-r1.png) \
Конфигурация для r2. \
![r2-static-route](imgs/DO2_5-8-r2.png) \
Итоговая таблица с маршрутами. \
![r1-static-table](imgs/DO2_5-9-r2.png)

Вывод команд `ip r list 10.10.0.0/18` и `ip r list 0.0.0.0/0` для ws11. \
![ws11-ip-r-list](imgs/DO2_5-8-ws11.png) \
  ```text
  Маршрут, заданный явно, имеет более высокий приоритет над заданным по умолчанием.
  Для сети 10.10.0.0\18 есть заданный маршрут, и при нахождении правила срабатывает этот маршрут.
  ```

### Построение списка маршрутизаторов
Результат запуска `sudo tcpdump -tnv -i enp0s8` на r1: \
![r1-tcpdump](imgs/DO2_5-10-r1.png) \
Список маршрутизаторов на пути от ws11 до ws21: \
![ws11-router-list](imgs/DO2_5-9-ws11.png) 

  ```text
  traceroute делает трассировку маршрута до указанного узла, отправляя серию пакетов. Это могут быть UDP по умолчанию, TCP, ICMP-пакеты. Их время жизни TTL = 1, и этот параметр увеличивается на единицу с каждой отправленной серией пакетов.
  
  TTL - это максимальное время жизни сетевого пакета. По факту, это счетчик, 
  который уменьшается на одну единицу при прохождении очередного хоста.
  
  На первом шаге с ttl=1 пакет не достигает нужного узла и возвращает ответ о не достижении цели. tracerout фиксирует время и посылает следующие пакеты с временем жизни 2,
  цикл повторяется до достижения узла.
  ```

### Использование протокола **ICMP** при маршрутизации
Вызов команды `sudo tcpdump -n -i enp0s8 icmp` на r1 \
![r1-tcpdump-icmp](imgs/DO2_5-11-r1.png) \
Вызов команды `ping -c 1 10.30.0.111` на ws11 \
![ws11-ping-invalid](imgs/DO2_5-10-ws11.png) 


## Part 6. Динамическая настройка IP с помощью **DHCP**

### R2 - WS21
#### R2
Файл */etc/dhcp/dhcpd.conf*: \
![r2-dhcpd-conf](imgs/DO2_6-1-r2.png) \
Файл */etc/resolv.conf*: \
![r2-resolv-conf](imgs/DO2_6-2-r2.png) \
Файл */etc/default/isc-dhcp-server*: \
![r2-isc-dhcp](imgs/DO2_6-4-r2.png) \
Запуск сервиса DHCP: \
![r2-dhcp-start](imgs/DO2_6-3-r2.png)

#### WS21
Файл *etc/netplan/00-installer-config.yaml*: \
![ws21-netplan-config](imgs/DO2_6-1-ws21.png) \
Вызов команды `ip a`: \
![ws21-new-ip](imgs/DO2_6-2-ws21.png) \
Пинг *ws22*: \
![ws21-new-ip](imgs/DO2_6-3-ws21.png) 

#### Запрос на смену IP с WS21
Было: \
![ws21-old-ip](imgs/DO2_6-2-ws21.png) \
Стало: \
`sudo dhclient enp0s8 -r`- освобождаем и снимаем резерв адреса для интерфейса \
`sudo dhclient enp0s8` резервируем и получаем новый адрес
![ws21-new-gen-ip](imgs/DO2_6-4-ws21.png) 

### R1 - WS11
#### R1
Файл */etc/dhcp/dhcpd.conf*: \
![r1-dhcpd-conf](imgs/DO2_6-1-r1.png) \
Файл */etc/resolv.conf*: \
![r1-resolv-conf](imgs/DO2_6-2-r1.png) \
Файл */etc/default/isc-dhcp-server*: \
![r1-isc-dhcp](imgs/DO2_6-3-r1.png) \
Запуск сервиса DHCP: \
![r1-dhcp-start](imgs/DO2_6-4-r1.png)

#### WS11
Файл *etc/netplan/00-installer-config.yaml*: \
![ws11-netplan-config](imgs/DO2_6-1-ws11.png) \
Вызов команды `ip a`: \
![ws11-new-ip](imgs/DO2_6-2-ws11.png) \
Пинг *r1*: \
![ws11-new-ip](imgs/DO2_6-3-ws11.png) \

---
---

## Part 7. **NAT**
### Содержание файла /etc/apache2/ports.conf
Для r1\
![apache_r1](imgs/DO2_7-1-r1.png)\
Для ws22 \
![apache_ws21](imgs/DO2_7-1-ws22.png)

### Запуск веб-сервера Apache
Для запуска сервера используется команда `service apache2 start`. \
Для r1\
![apache_start_r1](imgs/DO2_7-2-r1.png)\
Для ws22 \
![apache_start_ws21](imgs/DO2_7-2-ws22.png)

### Настройки вайервола для r2
![firewall_r2_settings](imgs/DO2_7-1-r2.png)\
![firewall_r2_apply](imgs/DO2_7-2-r2.png)\
Пинг r1 с ws22: \
![firewall_r2_ping](imgs/DO2_7-3-ws22.png)

### Разрешение маршрутизации всех пакетов протокола **ICMP**
![firewall_r2_settings_icmp](imgs/DO2_7-3-r2.png)\
![firewall_r2_apply_icmp](imgs/DO2_7-4-r2.png)\
Пинг r1 с ws22: \
![firewall_r2_ping_icmp](imgs/DO2_7-4-ws22.png)\

### Включение **SNAT** и **DNAT**
Нужно добавить:
1. **SNAT** - маскирование всех локальных ip из локальной сети, находящейся за r2 (по обозначениям из Части 5 - сеть 10.20.0.0)
2. **DNAT** на 8080 порт машины r2 и добавить к веб-серверу Apache, запущенному на ws22, доступ извне сети 

![firewall_r2_settings_nat](imgs/DO2_7-5-r2.png)\
![firewall_r2_apply_nat](imgs/DO2_7-6-r2.png)
#### Проверка соединение по TCP для **SNAT**
Для этого используется команда `sudo telnet 10.100.0.11 80` с ws22 \
![firewall_ws22_snat](imgs/DO2_7-5-ws22.png)\
#### Проверка соединение по TCP для **DNAT**
Для этого используется команда `sudo telnet 10.100.0.12 8080`с r1 \
![firewall_r1_dnat](imgs/DO2_7-3-r1.png)\

---
---

## Part 8. Дополнительно. Знакомство с **SSH Tunnels**

В качестве напоминания, вот так выглядит файервол r2\
![a_gentle_reminder](imgs/DO2_8-1-r2.png)\
Обновленный */etc/apache2/ports.conf* для ws22\
![apache_ws22](imgs/DO2_8-1-ws22.png) \
Перезапуск веб-сервера **Apache**
![apache_ws22_restart](imgs/DO2_8-2-ws22.png) \

### Получение доступа к ws22 с ws21
![ws21_access_to_ws22_1](imgs/DO2_8-1-ws21.png) \
![ws21_access_to_ws22_2](imgs/DO2_8-2-ws21.png) \
Проверка подключения с помощью telnet\
![ws21_access_to_ws22_telnet](imgs/DO2_8-3-ws21.png) \

### Получение доступа к ws22 с ws11
![ws11_access_to_ws22_1](imgs/DO2_8-1-ws11.png) \
![ws11_access_to_ws22_2](imgs/DO2_8-2-ws11.png) \
Проверка подключения с помощью telnet\
![ws11_access_to_ws22_telnet](imgs/DO2_8-3-ws11.png) \
